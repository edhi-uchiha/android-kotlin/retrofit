package com.enigma.sepotifay

import com.enigma.sepotifay.artist.ArtistRepository
import com.enigma.sepotifay.artist.ArtistService
import com.enigma.sepotifay.artist.ArtistViewModel
import com.enigma.sepotifay.config.RetrofitClient
import com.enigma.sepotifay.genre.GenreRepository
import com.enigma.sepotifay.genre.GenreService
import com.enigma.sepotifay.genre.GenreViewModel

class AppContainer {
    val genreService: GenreService = RetrofitClient.createRetrofit().create(GenreService::class.java)
    val genreRepository: GenreRepository = GenreRepository(genreService)
    val genreViewModel: GenreViewModel = GenreViewModel(genreRepository)

    val artistService: ArtistService = RetrofitClient.createRetrofit().create(ArtistService::class.java)
    val artistRepository: ArtistRepository = ArtistRepository(artistService)
    val artistViewModel: ArtistViewModel = ArtistViewModel(artistRepository)
}