package com.enigma.sepotifay

import android.app.Application

class MyApplication: Application() {

    val appContainer: AppContainer = AppContainer()
}