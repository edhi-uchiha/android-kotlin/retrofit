package com.enigma.sepotifay.genre

import com.enigma.sepotifay.dto.ResponseBuilder
import retrofit2.Call
import retrofit2.http.*

interface GenreService {

    //http://10.0.2.2:8008/genres/{id}
    @GET("$PATH/{id}")
    fun getById(@Path("id") id : String): Call<ResponseBuilder>

    @GET(PATH)
    fun getAll(): Call<ResponseBuilder>

    @POST(PATH)
    fun createGenre(@Body genre: Genre): Call<ResponseBuilder>

    @PUT(PATH)
    fun updateGenre(@Body genre: Genre): Call<ResponseBuilder>

    @DELETE("$PATH/{id}")
    fun removeGenre(@Path("id") id: String): Call<ResponseBuilder>

    companion object {
        const val PATH = "genres"
    }

}