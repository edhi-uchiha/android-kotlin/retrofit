package com.enigma.sepotifay.genre

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.enigma.sepotifay.R

class GenreAdapter(
    private var listItem: List<Genre>,
    private var listener: GenreListener
): RecyclerView.Adapter<GenreAdapter.GenreViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenreViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_genre, parent, false)
        return GenreViewHolder(view)
    }

    override fun onBindViewHolder(holder: GenreViewHolder, position: Int) {
        val genre: Genre = listItem[position]
        holder.bind(genre)

        holder.itemView.setOnClickListener{
            listener.onItemClicked(genre)
        }
        holder.detailImgBtn?.setOnClickListener {
            listener.onItemClicked(genre)
        }
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    class GenreViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        var name: TextView? = null
        var detailImgBtn : ImageButton? = null

        init {
            name = itemView.findViewById(R.id.genreName)
            detailImgBtn = itemView.findViewById(R.id.detailGenreBtn)
        }

        fun bind(genre: Genre){
            name?.text = genre.name
        }
    }

    interface GenreListener {
        fun onItemClicked(genre: Genre)
    }
}
