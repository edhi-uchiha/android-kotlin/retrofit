package com.enigma.sepotifay.genre

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel

class GenreViewModel(var repository: GenreRepository) : ViewModel(){

    val listOfGenres : LiveData<List<Genre>> = repository.listOfGenres

    fun getAll(){
        repository.getAll()
    }

    fun createGenre(genre: Genre){
        repository.create(genre)
    }

    fun updateGenre(genre: Genre){
        repository.update(genre)
    }

    fun deleteGenre(id: String){
        repository.delete(id)
    }
}