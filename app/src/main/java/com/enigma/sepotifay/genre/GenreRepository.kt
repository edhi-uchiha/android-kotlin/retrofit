package com.enigma.sepotifay.genre

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.enigma.sepotifay.dto.ResponseBuilder
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GenreRepository(private val genreService: GenreService) {

    var listOfGenres: MutableLiveData<List<Genre>> = MutableLiveData<List<Genre>>()
    val gson = GsonBuilder().create()

    fun getAll(){
        genreService.getAll().enqueue(object : Callback<ResponseBuilder> {
            override fun onResponse(
                call: Call<ResponseBuilder>,
                response: Response<ResponseBuilder>
            ) {
                if(response.code() == 200){
                    val resp = response.body()!!
                    listOfGenres.value = gson.fromJson(gson.toJson(resp.data), Array<Genre>::class.java).toList()
                    Log.d("genresRepo", resp.data.toString())
                }
            }

            override fun onFailure(call: Call<ResponseBuilder>, t: Throwable) {
                println(t.localizedMessage)
            }

        })
    }

    fun create(genre: Genre){
        genreService.createGenre(genre).enqueue(object : Callback<ResponseBuilder>{
            override fun onFailure(call: Call<ResponseBuilder>, t: Throwable) {
                TODO("Not yet implemented")
            }

            override fun onResponse(
                call: Call<ResponseBuilder>,
                response: Response<ResponseBuilder>
            ) {
                if(response.code() == 201){
                    Log.d("response", "Genre saved.")
                }
            }
        })
    }

    fun update(genre: Genre){
        genreService.updateGenre(genre).enqueue(object : Callback<ResponseBuilder>{
            override fun onResponse(
                call: Call<ResponseBuilder>,
                response: Response<ResponseBuilder>
            ) {
                if(response.code() == 200){
                    Log.d("response", "Genre updated.")
                }
            }

            override fun onFailure(call: Call<ResponseBuilder>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }

    fun delete(id: String){
        genreService.removeGenre(id).enqueue(object : Callback<ResponseBuilder>{
            override fun onResponse(
                call: Call<ResponseBuilder>,
                response: Response<ResponseBuilder>
            ) {
                TODO("Not yet implemented")
            }

            override fun onFailure(call: Call<ResponseBuilder>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }
}