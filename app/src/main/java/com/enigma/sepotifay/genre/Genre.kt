package com.enigma.sepotifay.genre

import android.os.Parcelable
import com.enigma.sepotifay.song.Song
import kotlinx.android.parcel.Parcelize

@Parcelize
class Genre(
    var id: String = "",
    var name: String,
    var songs: List<Song>?
) : Parcelable