package com.enigma.sepotifay.song

import android.os.Parcelable
import com.enigma.sepotifay.artist.Artist
import com.enigma.sepotifay.genre.Genre
import kotlinx.android.parcel.Parcelize

@Parcelize
class Song(
    var id: String,
    var title: String,
    var duration: Int,
    var releaseYear: Int,
    var artist: Artist,
    var genre: Genre
) : Parcelable {
}