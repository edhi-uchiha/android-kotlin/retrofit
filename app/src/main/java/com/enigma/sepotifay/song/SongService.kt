package com.enigma.sepotifay.song

import com.enigma.sepotifay.dto.ResponseBuilder
import retrofit2.Call
import retrofit2.http.*

interface SongService {

    @GET(PATH)
    fun getAll(): Call<ResponseBuilder>

    @GET("$PATH/{id}")
    fun getById(@Path("id") id: String): Call<ResponseBuilder>

    @POST(PATH)
    fun create(@Body song: Song): Call<ResponseBuilder>

    @PUT(PATH)
    fun update(@Body song: Song): Call<ResponseBuilder>

    @DELETE("$PATH/{id}")
    fun delete(@Path("id") id: String): Call<ResponseBuilder>

    companion object {
        const val PATH = "songs"
    }
}