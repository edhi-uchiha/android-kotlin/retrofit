package com.enigma.sepotifay.artist

import android.os.Parcelable
import com.enigma.sepotifay.song.Song
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class Artist(
    var id: String = "",
    var name: String,
    var gender: String,
    var biography: String,
    var photo: String,
    var debutYear: String,
    var songs: List<Song>
) : Parcelable