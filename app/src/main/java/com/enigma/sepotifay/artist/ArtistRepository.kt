package com.enigma.sepotifay.artist

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.enigma.sepotifay.dto.ResponseBuilder
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ArtistRepository (val artistService: ArtistService) {

    var artist: MutableLiveData<Artist> = MutableLiveData<Artist>()
    var listArtist: MutableLiveData<List<Artist>> = MutableLiveData<List<Artist>>()
    val gson = Gson()

    fun getArtistById(id: String) {
        Log.d("repo id", id)
        artistService.getById(id).enqueue(object : Callback<ResponseBuilder> {
            override fun onFailure(call: Call<ResponseBuilder>, t: Throwable) {
                println(t.localizedMessage)
            }

            override fun onResponse(call: Call<ResponseBuilder>, response: Response<ResponseBuilder>) {
                if(response.code() == 200){
                    val artistResponse = response.body()!!
                    artistResponse.data = gson.fromJson(gson.toJson(artistResponse.data), Artist::class.java)
                    artist.value = artistResponse.data as Artist?
                    Log.d("resp", gson.toJson(artistResponse))
                }
            }
        })
    }

    fun getAll(){
        artistService.getAll().enqueue(object : Callback<ResponseBuilder>{
            override fun onResponse(
                call: Call<ResponseBuilder>,
                response: Response<ResponseBuilder>
            ) {
                if(response.code() == 200){
                    val resp = response.body()!!
//                    resp.data = listOf(gson.fromJson(gson.toJson(resp.data), Artist::class.java))
                    listArtist.value = resp.data as List<Artist>
                    Log.d("resp", gson.toJson(resp))
                }
            }


            override fun onFailure(call: Call<ResponseBuilder>, t: Throwable) {
                println(t.localizedMessage)
            }

        })
    }
}