package com.enigma.sepotifay.artist

import android.graphics.BitmapFactory
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.enigma.sepotifay.R
import com.enigma.sepotifay.genre.Genre
import java.io.InputStream
import java.net.URL


class ArtistAdapter(val listItem: List<Genre>): RecyclerView.Adapter<ArtistAdapter.ArtistViewHolder>(){

    private var listOfArtist = mutableListOf<Artist>()
    private var listOfGenre = mutableListOf<Genre>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArtistViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.list_item_artist,
            parent,
            false
        )
        return ArtistViewHolder(view)
    }

    override fun onBindViewHolder(holder: ArtistViewHolder, position: Int) {
        val item = listItem[position]
        holder.artistName.text = item.name
//        holder.biography.text = item.biography

        val bitmap = BitmapFactory.decodeStream(URL("https://www.pexels.com/photo/brown-rocks-during-golden-hour-2014422/").getContent() as InputStream)
        holder.photo.setImageBitmap(bitmap)
        Log.d("artist", item.toString())
        holder.itemView.setOnClickListener{
//            ArtistListener.OnItemClicked(item)
        }
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    fun setArtists(listOfArtist: List<Artist>){
        this.listOfArtist = listOfArtist.toMutableList()
        notifyDataSetChanged()
    }

    fun setGenres(listofGenre: List<Genre>){
        this.listOfGenre = listOfGenre.toMutableList()
        notifyDataSetChanged()
    }

    class ArtistViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        var artistName = itemView.findViewById<TextView>(R.id.name)
        var biography = itemView.findViewById<TextView>(R.id.biography)
        var photo = itemView.findViewById<ImageView>(R.id.imageView)
    }

    interface ArtistListener {
        fun OnItemClicked(artist: Artist)
    }
}