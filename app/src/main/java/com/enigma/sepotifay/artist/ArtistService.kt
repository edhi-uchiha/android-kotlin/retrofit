package com.enigma.sepotifay.artist

import com.enigma.sepotifay.dto.ResponseBuilder
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ArtistService {

    //http://10.0.2.2:8008/artists/{id}
    @GET("$PATH/{id}")
    fun getById(@Path("id") id : String): Call<ResponseBuilder>

    @GET(PATH)
    fun getAll(): Call<ResponseBuilder>

    companion object {
        const val PATH = "artists"
    }

}