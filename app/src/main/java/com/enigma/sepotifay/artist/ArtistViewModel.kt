package com.enigma.sepotifay.artist

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.enigma.sepotifay.config.RetrofitClient

class ArtistViewModel(var repository: ArtistRepository) : ViewModel(){

    val artist: LiveData<Artist> = repository.artist
    val listArtist : LiveData<List<Artist>> = repository.listArtist

    fun getArtist(id: String){
        repository.getArtistById(id)
    }

    fun getAll(){
        repository.getAll()
    }
}