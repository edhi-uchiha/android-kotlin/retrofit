package com.enigma.sepotifay.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.enigma.sepotifay.AppContainer
import com.enigma.sepotifay.MyApplication
import com.enigma.sepotifay.R
import com.enigma.sepotifay.genre.Genre
import com.enigma.sepotifay.genre.GenreViewModel
import kotlinx.android.synthetic.main.activity_genre_form.*

class GenreFormActivity : AppCompatActivity() {
    private lateinit var appContainer: AppContainer
    val GENRE_EXTRA = "edit_genre"
    private lateinit var genre: Genre
    private var isUpdate: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_genre_form)

        appContainer = (application as MyApplication).appContainer

        removeBtn.visibility = View.GONE

        saveBtn.setOnClickListener {
            saveGenre()
        }

        if(intent.getParcelableExtra<Genre>(GENRE_EXTRA) !== null){
            isUpdate = true
            removeBtn.visibility = View.VISIBLE
            genre = intent.getParcelableExtra(GENRE_EXTRA)!!
            editTextGenderName.setText(genre.name)
            Log.d("songs", genre.toString())
        }

        removeBtn.setOnClickListener {
            deleteGenre(genre.id)
        }

    }

    private fun saveGenre(){
        if(isUpdate){
            appContainer.genreViewModel.updateGenre(Genre(id = genre.id,name = editTextGenderName.text.toString(), songs = null))
            Toast.makeText(this, "Gerne successfully updated", Toast.LENGTH_LONG).show()
        } else {
            appContainer.genreViewModel.createGenre(Genre(name = editTextGenderName.text.toString(), songs = null))
            Toast.makeText(this, "Gerne successfully created", Toast.LENGTH_LONG).show()
        }
        var intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    private fun deleteGenre(id: String){
        appContainer.genreViewModel.deleteGenre(id)
        Toast.makeText(this, "Gerne successfully deleted", Toast.LENGTH_LONG).show()
    }

}