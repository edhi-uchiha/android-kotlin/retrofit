package com.enigma.sepotifay.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.Fragment
import com.enigma.sepotifay.screens.HomeFragment
import com.enigma.sepotifay.R
import com.enigma.sepotifay.screens.SearchFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    lateinit var bottomNavigationView : BottomNavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView = findViewById(R.id.bottomTabNavigation)
        bottomNavigationView.setOnNavigationItemSelectedListener(onNavigationSelectedListener)
        val homeFragment = HomeFragment.newInstance("", "")
        openFragment(homeFragment)

    }

    private val onNavigationSelectedListener= BottomNavigationView.OnNavigationItemSelectedListener { item: MenuItem ->
        when (item.itemId){
            R.id.navigation_home -> {
                val homeFragment = HomeFragment.newInstance("", "")
                openFragment(homeFragment)
                true
            }
            R.id.navigation_search -> {
                val searchFragment = SearchFragment.newInstance("", "")
                openFragment(searchFragment)
                true
            }
            else -> false
        }
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.content, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

}