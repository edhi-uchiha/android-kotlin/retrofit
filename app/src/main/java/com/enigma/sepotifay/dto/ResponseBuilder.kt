package com.enigma.sepotifay.dto

import com.google.gson.annotations.SerializedName

class Status(
    @SerializedName("code")
    var code: String,

    @SerializedName("message")
    var message: String
)

class ResponseBuilder(
    var status: Status,
    var data: Any
)